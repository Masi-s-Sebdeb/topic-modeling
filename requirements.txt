pandas==1.5.2
numpy==1.23.5
plotly==5.11.0
pyLDAvis==3.3.1
scikit-learn==1.1.3